<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $table = "clientes";
    protected $fillable = [
        "id_cliente", "id_endereco", "id_endereco_tipo", "nome",
        "cnpj", "telefone", "responsavel", "email"
      ];
    protected $primaryKey = 'id_cliente';

    protected $casts = [
        'id_endereco' => 'array'
    ];

    public function enderecos()
    {
        return $this->hasMany(Endereco::class, 'id_cliente');
    }

    public function enderecotipo()
    {
        return $this->morphToMany(Enderecotipo::class, 'id_endereco_tipo');
    }
}
