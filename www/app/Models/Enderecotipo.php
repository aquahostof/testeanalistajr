<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enderecotipo extends Model
{
    use HasFactory;

    protected $table = "endereco_tipo";
    protected $fillable = [
        "id_endereco_tipo", "descricao"
      ];
    protected $primaryKey = 'id_endereco_tipo';

    public function enderecos()
    {
        return $this->hasMany(Endereco::class, 'id_endereco_tipo');
    }
}
