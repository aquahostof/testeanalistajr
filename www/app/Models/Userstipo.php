<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Userstipo extends Model
{
    use HasFactory;

    protected $table = "users_tipo";
    protected $fillable = [
        "id_users_tipo", "descricao"
      ];
    protected $primaryKey = 'id_users_tipo';
    public $timestamps = false;

    public function usuarios()
    {
        return $this->hasMany(User::class, 'id_users_tipo');
    }

}
