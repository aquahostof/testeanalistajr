<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    use HasFactory;

    protected $table = "enderecos";
    protected $fillable = [
        "id_endereco", "id_cliente", "id_endereco_tipo", "cep", "logradouro",
        "numero", "complemento", "bairro", "cidade", "estado"
      ];
    protected $primaryKey = 'id_endereco';

    public function clientes()
    {
        return $this->belongsTo(Cliente::class, 'id_cliente');
    }
    public function enderecotipo()
    {
        return $this->belongsTo(Enderecotipo::class, 'id_endereco_tipo');
    }
}
