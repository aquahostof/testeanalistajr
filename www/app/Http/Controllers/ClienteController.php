<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Endereco;
use App\Models\Enderecotipo;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view', Usuario::class);
        $clientes = Cliente::all();
        return view('clientes.index', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Usuario::class);
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Usuario::class);
        try {
            $dados = $request->all();
            $cliente = Cliente::create($dados);
            return redirect()->route('cliente.endereco', $cliente->id_cliente)->with('status', 'Cliente cadastrado com sucesso!');
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => "Erro ao cadastrar cliente",
                'error' => $th
            ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Usuario::class);
        $clientes = Cliente::find($id);
        $enderecos = Endereco::all();
        $enderecotipo = Enderecotipo::all();
        return view('clientes.show', compact('clientes', 'enderecos', 'enderecotipo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Usuario::class);
        $clientes = Cliente::find($id);
        $enderecos = Endereco::all();
        $enderecotipo = Enderecotipo::all();
        return view('clientes.edit', compact('clientes', 'enderecos', 'enderecotipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Usuario::class);
        $dados = $request->all();
        Cliente::find($id)->update($dados);
        return redirect()->route('clientes.index')->with('success', "Cliente atualizado com sucesso!");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', Usuario::class);
        try {
            Cliente::find($id)->delete();
            return redirect()->route('clientes.index')->with('success', "Cliente excluido com sucesso!");
        } catch (\Throwable $th) {
            return response()->json([
                'success' =>false,
                'mensage' => "Erro ao deletar cliente"
            ]);
        }
    }
}
