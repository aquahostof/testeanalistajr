<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Userstipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        $tipo = Userstipo::all();
        return view('usuarios.index', compact('usuarios', 'tipo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipo = Userstipo::all();
        return view('usuarios.create', compact('tipo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dados = $request->all();
        $dados ['password'] = Hash::make($request->password);
        User::create($dados);
        return redirect()->route('usuarios.index')->with('success', "Usuário Cadastrado com sucesso!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipos = Userstipo::all();
        $usuarios = User::find($id);
        return view('usuarios.edit', compact('tipos', 'usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dados = $request->all();
        $dados ['password'] = Hash::make($request->password);
        User::find($id)->update($dados);
        return redirect()->route('usuarios.index')->with('success', "Usuário atualizado com sucesso!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            User::find($id)->delete();
            return response()->json(['success'=>true, 'message'=> "Usuário deletado com sucesso!"], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'success' =>false,
                'mensage' => "Erro ao deletar usuário"
            ]);
        }
    }
}
