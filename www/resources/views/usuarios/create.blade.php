@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="header-expand justify-content-lg-end text-lg-right">
                    </div>

                    <div class="card-body">
                        <form class="row g-3" action="{{route('usuarios.store')}}" method="POST">
                            @csrf
                            <div class="col-md-6">
                                <label for="name" class="form-label">Nome</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Nome">
                            </div>
                            <div class="col-md-6">
                                <label for="email" class="form-label">E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">
                            </div>
                            <div class="col-4">
                                <label for="password" class="form-label">Senha</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Senha">
                            </div>
                            <div class="col-4">
                                <label for="confirm-password" class="form-label">Confirmar senha</label>
                                <input type="password" class="form-control" id="confirm-password" placeholder="Confirmar senha">
                            </div>
                            <div class="col-md-4">
                                <label for="inputCity" class="form-label">Permissões</label>
                                <select id="id_users_tipo" name="id_users_tipo" class="form-control">
                                    @foreach ($tipo as $item)
                                        <option value="{{ $item->id_users_tipo }}">
                                            {{ $item->descricao }}
                                        </option>
                                    @endforeach
                                </select>
                                <br>
                            </div>
                            <div class="modal-footer col-12">
                                <button type="submit" class="btn btn-primary">Adicionar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
