@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="header-expand justify-content-lg-end text-lg-right">
                        @can('create', App\Usuario::class)
                            <a class="btn btn-success" href="{{route('usuarios.create')}}"><i class="fa fa-user-plus"></i>
                                Adicionar novo usuario</a>
                        @endcan
                    </div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif

                        <table id="mytable" class="table">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Nome</th>
                                    <th scope="col">E-mail</th>
                                    <th scope="col">Permissões</th>
                                    <th scope="col">Ações</th>
                                </tr>
                            </thead>
                            @foreach ($usuarios as $usuario)
                                <tbody>
                                    <tr>
                                        <td>{{ $usuario->id }}</td>
                                        <td>{{ $usuario->name }}</td>
                                        <td>{{ $usuario->email }}</td>
                                        @foreach ($tipo as $item)
                                            @if ($usuario->id_users_tipo == $item->id_users_tipo)
                                                <td>{{ $item->descricao }}</td>
                                            @endif
                                        @endforeach
                                        <td>
                                            <div class="btn-group btn-group-sm">
                                                @can('update', App\Usuario::class)
                                                    <a class="btn btn-success"
                                                        href="{{ route('usuarios.edit', $usuario->id) }}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endcan
                                                @can('delete', App\Usuario::class)
                                                    <a class="btn btn-danger delete-user" data-usuarios="{{ $usuario->id }}">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#mytable').DataTable();
        });
        //Método Delete//
        $(document).on('click', '.delete-user', function(e) {
            e.preventDefault();
            if (confirm('Tem certeza que deseja excluir?')) {
                var id = $(this).data('usuarios');
                var $bt = $(this);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'delete',
                    dataType: "JSON",
                    url: 'usuarios/' + id, //rota que deleta
                    beforeSend: function() {}
                }).done(function(response) {
                    console.log(response);
                    try {
                        //response = JSON.parse(response);
                        if (response.success) {
                            var $tr = $bt.closest('tr');
                            $tr.addClass('animated fadeOutLeft');
                            setTimeout(function() {
                                $tr.remove();
                            }, 1000);
                        } else {
                            alert(response.message);
                        }
                    } catch (e) {
                        alert('Ocorreu um erro desconhecido, por favor tente novamente mais tarde.',
                            'Erro!');
                    }
                });
            }
        });

    </script>
@endsection
