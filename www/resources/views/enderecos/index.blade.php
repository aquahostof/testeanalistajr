@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="header-expand justify-content-lg-end text-lg-right">
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table id="mytable" class="table">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Empresa</th>
                                    <th scope="col">CEP</th>
                                    <th scope="col">Logradouro</th>
                                    <th scope="col">Cidade</th>
                                    <th scope="col">Ações</th>
                                </tr>
                            </thead>
                            @foreach ($enderecos as $endereco)
                                <tbody>
                                    <tr>
                                        <td>{{ $endereco->id_endereco }}</td>
                                        @foreach ($clientes as $cliente)
                                            @if ($endereco->id_cliente == $cliente->id_cliente)
                                                <td>{{ $cliente->nome }}</td>
                                            @endif
                                        @endforeach
                                        <td>{{ $endereco->cep }}</td>
                                        <td>{{ $endereco->logradouro }}</td>
                                        <td>{{ $endereco->cidade }}</td>
                                        <td>
                                            <div class="btn-group btn-group-sm">
                                                @can('delete', App\Usuario::class)
                                                    <a class="btn btn-danger delete-user"
                                                        data-enderecos="{{ $endereco->id_endereco }}">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL CADASTRO-->
    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="clienteForm" name="clienteForm" class="form-horizontal">
                        <input type="hidden" name="id_cliente" id="id_cliente">
                        <div class="form-group">
                            <label for="nome" class="col-sm-2 control-label">Empresa</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Enter Name"
                                    value="{{ old('nome') }}" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Details</label>
                            <div class="col-sm-12">
                                <textarea id="detail" name="detail" required="" placeholder="Enter Details"
                                    class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#mytable').DataTable();
        });
        //Método Delete//
        $(document).on('click', '.delete-user', function(e) {
            e.preventDefault();
            if (confirm('Tem certeza que deseja excluir?')) {
                var id_endereco = $(this).data('enderecos');
                var $bt = $(this);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'delete',
                    dataType: "JSON",
                    url: 'enderecos/' + id_endereco, //rota que deleta
                    beforeSend: function() {}
                }).done(function(response) {
                    console.log(response);
                    try {
                        //response = JSON.parse(response);
                        if (response.success) {
                            var $tr = $bt.closest('tr');
                            $tr.addClass('animated fadeOutLeft');
                            setTimeout(function() {
                                $tr.remove();
                            }, 1000);
                        } else {
                            alert(response.message);
                        }
                    } catch (e) {
                        alert('Ocorreu um erro desconhecido, por favor tente novamente mais tarde.',
                            'Erro!');
                    }
                });
            }
        });

    </script>


@endsection
