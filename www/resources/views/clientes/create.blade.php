@extends('layouts.app')

@section('content')
    @if (session('message'))
        <div class="alert alert-success" role="alert">
            {{ session('message') }}
        </div>
    @endif
    <div class="card">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home">Empresa</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" @if (session('control_aba', '1')) {{ 'class="active"' }} @endif data-toggle="tab" href="#menu1">Endereços</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active container" id="home">
                <div class="card-body">
                    <form method="POST" action="{{ route('clientes.store') }}" class="form-horizontal row">
                        @csrf
                        <div class="col-md-6">
                            <label for="nome" class="control-label">Empresa</label>
                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome da Empresa"
                                value="" maxlength="50" required="">
                        </div>
                        <div class="col-md-3">
                            <label for="cnpj" class="control-label">CNPJ</label>
                            <input type="text" class="form-control cnpj" id="cnpj" name="cnpj" placeholder="CNPJ" value=""
                                maxlength="50" required="">
                        </div>
                        <div class="col-md-3">
                            <label for="telefone" class="control-label">Telefone</label>
                            <input type="text" class="form-control telefone" id="telefone" name="telefone"
                                placeholder="Telefone" value="" maxlength="50" required="">
                        </div>
                        <div class="col-md-6">
                            <label for="responsavel" class="control-label">Responsável</label>
                            <input type="text" class="form-control" id="responsavel" name="responsavel"
                                placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                        <div class="col-md-6">
                            <label for="email" class="control-label">E-mail</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="E-mail" value=""
                                maxlength="50" required="">
                            <br>
                        </div>
                        <div class="modal-footer col-sm-offset-2 col-sm-12">
                            <button type="submit" class="btn btn-primary">Adicionar</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane container" @if (session('control_aba', '1')) {{ 'class="active"' }} @endif id="menu1">
                <div class="card-body">
                    <form method="POST" name="id_cliente" action="{{ route('clientes.store') }}"
                        class="form-horizontal row">
                        @csrf
                        <input type="hidden" name="id_cliente[]" id="id_cliente">
                        <div class="col-md-3">
                            <label for="cep" class="control-label">CEP</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control cep" id="cep" name="cep" placeholder="CEP" value=""
                                    maxlength="50" required="">
                            </div>
                        </div>
                        <div class="col-md-7">
                            <label for="logradouro" class="control-label">Logradouro</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="logradouro" name="logradouro"
                                    placeholder="Logradouro" value="" maxlength="50" required="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label for="numero" class="control-label">Número</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="numero" name="numero" placeholder="Nº" value=""
                                    maxlength="50" required="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="complemento" class="control-label">Complemento</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="complemento" name="complemento"
                                    placeholder="Complemento" value="" maxlength="50" required="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="bairro" class="control-label">Bairro</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro"
                                    value="" maxlength="50" required="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="cidade" class="control-label">Cidade</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Cidade"
                                    value="" maxlength="50" required="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="estado" class="control-label">Estado</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="estado" name="estado" placeholder="Estado"
                                    value="" maxlength="50" required="">
                            </div>
                        </div>
                        <div class="modal-footer col-sm-offset-2 col-sm-12">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Adicionar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //exibir modal adicionar endereços//
            $('#createNewCliente').click(function() {
                $('#saveBtn').val("create-cliente");
                $('#id_cliente').val('');
                $('#clienteForm').trigger("reset");
                $('#modelHeading').html("Adicionar novo endereço");
                $('#ajaxModel').modal('show');
            });

            $('#saveBtn').click(function(e) {
                e.preventDefault();
                $(this).html('Sending..');

                $.ajax({
                    data: $('#clienteForm').serialize(),
                    url: "{{ route('clientes.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {

                        $('#clienteForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        table.draw();

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Salvar Alterações');
                    }
                });
            });
        });

        //DataTable da Tabela//
        $(document).ready(function() {
            $('.tabela').DataTable({
                pageLength: 10,
                responsive: true,
            });
        });
        //Campos com mascara//
        $(document).ready(function() {
            $('.telefone').mask("(00) 00000-0000", {
                placeholder: "(__) _ ____-____"
            });
            $('.cnpj').mask("00.000.000/0000-00", {
                placeholder: "__.___.___/____-__"
            });
            $('.cep').mask("00.000-000", {
                placeholder: "__.___-___"
            });
        });
        //carregamento do endereço automatico//
        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#logradouro").val("");
                $("#complemento").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#estado").val("");
            }

            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if (validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#logradouro").val("...");
                        $("#complemento").val("");
                        $("#bairro").val("");
                        $("#cidade").val("...");
                        $("#estado").val("...");
                        $("#complemento").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#logradouro").val(dados.logradouro);
                                $("#complemento").val(dados.complemento);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#estado").val(dados.uf);

                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>

@endsection
