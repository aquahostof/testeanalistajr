@extends('layouts.app')

@section('content')
<div class="callout">
    <h3>Dados Empresarial</h3>
    <hr>
    <div class="row">
        <div class="col-lg-6 row">
            <div class="col-lg-4">
                <h4>Razão Social</h4>
                <p>{{$clientes->nome}}</p>
            </div>
            <div class="col-lg-4">
                <h4>CNPJ</h4>
                <p>{{$clientes->cnpj}}</p>
            </div>
            <div class="col-lg-4">
                <h4>Telefone</h4>
                <p>{{$clientes->telefone}}</p>
            </div>
            <div class="col-lg-4">
                <h4>Responsável</h4>
                <p>{{$clientes->responsavel}}</p>
            </div>
            <div class="col-lg-4">
                <h4>E-mail</h4>
                <p>{{$clientes->email}}</p>
            </div>
        </div>
        <div class="col-lg-6 row">
            <table id="table-endereco" class="table data-table clientes">
                <thead>
                    <tr>
                        <th scope="col">CEP</th>
                        <th scope="col">Logradouro</th>
                        <th scope="col">Cidade</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Tipo</th>
                    </tr>
                </thead>
                @foreach ($enderecos as $endereco)
                    @if ($clientes->id_cliente == $endereco->id_cliente)
                        <tbody>
                            <tr>
                                <th scope="row">{{ $endereco->cep }}</th>
                                <td>{{ $endereco->logradouro, $endereco->numero, $endereco->bairro }}</td>
                                <td>{{ $endereco->cidade }}</td>
                                <td>{{ $endereco->estado }}</td>
                                @foreach ($enderecotipo as $tipo)
                                    @if ($endereco->id_endereco_tipo == $tipo->id_endereco_tipo)
                                        <td value="{{ $tipo->id_endereco_tipo }}">
                                            <span class="@if ($tipo->id_endereco_tipo ===
                                                1) ? 'badge bg-success' : '' @endif">{{ $tipo->descricao }}</span>
                                        </td>
                                    @endif
                                @endforeach
                            </tr>
                        </tbody>
                    @endif
                @endforeach
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#table-endereco').DataTable();
    });
    //Método Delete//
    $(document).on('click', '.delete-user', function(e) {
        e.preventDefault();
        if (confirm('Tem certeza que deseja excluir?')) {
            var id_endereco = $(this).data('enderecos');
            var $bt = $(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'delete',
                dataType: "JSON",
                url: 'enderecos/' + id_endereco, //rota que deleta
                beforeSend: function() {}
            }).done(function(response) {
                console.log(response);
                try {
                    //response = JSON.parse(response);
                    if (response.success) {
                        var $tr = $bt.closest('tr');
                        $tr.addClass('animated fadeOutLeft');
                        setTimeout(function() {
                            $tr.remove();
                        }, 1000);
                    } else {
                        alert(response.message);
                    }
                } catch (e) {
                    alert('Ocorreu um erro desconhecido, por favor tente novamente mais tarde.',
                        'Erro!');
                }
            });
        }
    });

</script>

@endsection
