@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('status'))
        <div class="toast align-items-center text-white bg-primary border-0" role="alert" aria-live="assertive"
            aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {{ session('status') }}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>
    @endif
    <div class="card">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home">Dados da Empresa</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('clientes/{id_cliente}/edit/#endereco') ? 'active' : '' }}"
                    data-toggle="tab" href="#endereco">Endereços</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane container active" id="home">
                <div class="card-body">
                    <form method="POST" action="{{ route('clientes.store') }}" class="row g-3">
                        @csrf
                        <div class="col-sm-6">
                            <label for="nome" class="form-label">Empresa</label>
                            <input type="text" value="{{ $clientes->nome }}" class="form-control" id="nome" name="nome"
                                placeholder="Nome da Empresa">
                        </div>
                        <div class="col-sm-3">
                            <label for="cnpj" class="form-label">CNPJ</label>
                            <input type="text" value="{{ $clientes->cnpj }}" class="form-control cnpj" id="cnpj"
                                name="cnpj" placeholder="CNPJ">
                        </div>
                        <div class="col-sm-3">
                            <label for="telefone" class="form-label">Telefone</label>
                            <input type="text" value="{{ $clientes->telefone }}" class="form-control telefone"
                                id="telefone" name="telefone" placeholder="Telefone">
                        </div>
                        <div class="col-sm-6">
                            <label for="responsavel" class="form-label">Responsável</label>
                            <input type="text" value="{{ $clientes->responsavel }}" class="form-control" id="responsavel"
                                name="responsavel" placeholder="Enter Name">
                        </div>
                        <div class="col-sm-6">
                            <label for="email" class="form-label">E-mail</label>
                            <input type="text" value="{{ $clientes->email }}" class="form-control" id="email" name="email"
                                placeholder="E-mail">
                        </div>
                        <div class="col-sm-offset-2 col-md-12">
                            <br>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Adicionar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane container" id="endereco">
                <div class="card-body">
                    <form method="POST" action="{{ route('enderecos.update', $clientes->id_cliente) }}" class="form-horizontal row">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id_cliente" id="id_cliente" value="{{ $clientes->id_cliente }}">
                        <div class="col-md-3">
                            <label for="cep" class="form-label">CEP</label>
                            <input type="text" class="form-control cep" id="cep" name="cep" placeholder="CEP" value=""
                                required>
                        </div>
                        <div class="col-md-7">
                            <label for="logradouro" class="form-label">Logradouro</label>
                            <input type="text" class="form-control" id="logradouro" name="logradouro"
                                placeholder="Logradouro" value="" required>
                        </div>
                        <div class="col-md-2">
                            <label for="numero" class="form-label">Número</label>
                            <input type="text" class="form-control" id="numero" name="numero" placeholder="Nº" value=""
                                required>
                        </div>
                        <div class="col-md-8">
                            <label for="complemento" class="form-label">Complemento</label>
                            <input type="text" class="form-control" id="complemento" name="complemento"
                                placeholder="Complemento" value="">
                        </div>
                        <div class="col-md-4">
                            <label for="complemento" class="form-label">Tipo de Endereço</label>
                            <select id="id_endereco_tipo" name="id_endereco_tipo" class="form-control">
                                <option selected>Tipo...</option>
                                @foreach ($enderecotipo as $tipo)
                                    <option value="{{ $tipo->id_endereco_tipo }}">{{ $tipo->descricao }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="bairro" class="form-label">Bairro</label>
                            <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro" value=""
                                required>
                        </div>
                        <div class="col-md-4">
                            <label for="cidade" class="form-label">Cidade</label>
                            <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Cidade" value=""
                                required>
                        </div>
                        <div class="col-md-4">
                            <label for="estado" class="form-label">Estado</label>
                            <input type="text" class="form-control" id="estado" name="estado" placeholder="Estado" value=""
                                required>
                            <br>
                        </div>
                        <div class="modal-footer col-sm-offset-2 col-sm-12">
                            <button type="submit" class="btn btn-primary">Adicionar</button>
                        </div>
                    </form>
                    <table id="#table-endereco" class="table data-table clientes">
                        <thead>
                            <tr>
                                <th scope="col">CEP</th>
                                <th scope="col">Logradouro</th>
                                <th scope="col">Cidade</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        @foreach ($enderecos as $endereco)
                            @if ($clientes->id_cliente == $endereco->id_cliente)
                                <tbody>
                                    <tr>
                                        <th scope="row">{{ $endereco->cep }}</th>
                                        <td>{{ $endereco->logradouro, $endereco->numero, $endereco->bairro }}</td>
                                        <td>{{ $endereco->cidade }}</td>
                                        <td>{{ $endereco->estado }}</td>
                                        @foreach ($enderecotipo as $tipo)
                                            @if ($endereco->id_endereco_tipo == $tipo->id_endereco_tipo)
                                                <td value="{{ $tipo->id_endereco_tipo }}">
                                                    <span class="@if ($tipo->id_endereco_tipo ===
                                                        1) ? 'badge bg-success' : '' @endif">{{ $tipo->descricao }}</span>
                                                </td>
                                            @endif
                                        @endforeach
                                        <td class="btn-group btn-group-sm">
                                            @can('view', App\Usuario::class)
                                                <a class="btn btn-primary" href="{{route('enderecos.show', $endereco->id_endereco)}}"><i class="fa fa-eye"></i></a>
                                            @endcan
                                            @can('delete', App\Usuario::class)
                                                <a class="btn btn-danger delete-user"
                                                    data-enderecos="{{ $endereco->id_endereco }}">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                </tbody>
                            @endif
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#table-endereco').DataTable();
        });
        //Método Delete//
        $(document).on('click', '.delete-user', function(e) {
            e.preventDefault();
            if (confirm('Tem certeza que deseja excluir?')) {
                var id_endereco = $(this).data('enderecos');
                var $bt = $(this);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'delete',
                    dataType: "JSON",
                    url: 'enderecos/' + id_endereco, //rota que deleta
                    beforeSend: function() {}
                }).done(function(response) {
                    console.log(response);
                    try {
                        //response = JSON.parse(response);
                        if (response.success) {
                            var $tr = $bt.closest('tr');
                            $tr.addClass('animated fadeOutLeft');
                            setTimeout(function() {
                                $tr.remove();
                            }, 1000);
                        } else {
                            alert(response.message);
                        }
                    } catch (e) {
                        alert('Ocorreu um erro desconhecido, por favor tente novamente mais tarde.',
                            'Erro!');
                    }
                });
            }
        });

        //Campos com mascara//
        $(document).ready(function() {
            $('.telefone').mask("(00) 00000-0000", {
                placeholder: "(__) _ ____-____"
            });
            $('.cnpj').mask("00.000.000/0000-00", {
                placeholder: "__.___.___/____-__"
            });
            $('.cep').mask("00.000-000", {
                placeholder: "__.___-___"
            });
        });
        //carregamento do endereço automatico//
        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#logradouro").val("");
                $("#complemento").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#estado").val("");
            }

            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if (validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#logradouro").val("...");
                        $("#complemento").val("");
                        $("#bairro").val("");
                        $("#cidade").val("...");
                        $("#estado").val("...");
                        $("#complemento").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#logradouro").val(dados.logradouro);
                                $("#complemento").val(dados.complemento);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#estado").val(dados.uf);

                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>

@endsection
