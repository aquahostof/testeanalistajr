@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="header-expand justify-content-lg-end text-lg-right">
                        @can('create', App\Usuario::class)
                        <a class="btn btn-success" href="{{ route('clientes.create') }}"><i class="fa fa-user-plus"></i>
                            Adicionar novo cliente</a>
                        @endcan
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table id="mytable" class="table">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Empresa</th>
                                    <th scope="col">CNPJ</th>
                                    <th scope="col">Telefone</th>
                                    <th scope="col">Responsável</th>
                                    <th scope="col">Ações</th>
                                </tr>
                            </thead>
                            @foreach ($clientes as $cliente)
                                <tbody>
                                    <tr>
                                        <td>{{ $cliente->id_cliente }}</td>
                                        <td>{{ $cliente->nome }}</td>
                                        <td>{{ $cliente->cnpj }}</td>
                                        <td>{{ $cliente->telefone }}</td>
                                        <td>{{ $cliente->responsavel }}</td>
                                        <td>
                                            <div class="btn-group btn-group-sm">
                                                @can('view', App\Usuario::class)
                                                    <a class="btn btn-primary"
                                                        href="{{ route('clientes.show', $cliente->id_cliente) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                @endcan
                                                @can('update', App\Usuario::class)
                                                    <a class="btn btn-success"
                                                        href="{{ route('clientes.edit', $cliente->id_cliente) }}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endcan
                                                @can('delete', App\Usuario::class)
                                                    <a class="btn btn-danger delete-user"
                                                        data-clientes="{{ $cliente->id_cliente }}">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL CADASTRO-->
    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="clienteForm" name="clienteForm" class="form-horizontal">
                        <input type="hidden" name="id_cliente" id="id_cliente">
                        <div class="form-group">
                            <label for="nome" class="col-sm-2 control-label">Empresa</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Enter Name"
                                    value="{{ old('nome') }}" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Details</label>
                            <div class="col-sm-12">
                                <textarea id="detail" name="detail" required="" placeholder="Enter Details"
                                    class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#mytable').DataTable();
        });

        $('#createNewCliente').click(function() {
            $('#saveBtn').val("create-cliente");
            $('#id_cliente').val('');
            $('#clienteForm').trigger("reset");
            $('#modelHeading').html("Adicionar novo cliente");
            $('#ajaxModel').modal('show');
        });

        $('body').on('click', '.editCliente', function() {
            var id_cliente = $(this).data('id_cliente');
            $.get("{{ route('clientes.index') }}" + '/' + id_cliente + '/edit', function(data) {
                $('#modelHeading').html("Editar Cliente");
                $('#saveBtn').val("edit-user");
                $('#ajaxModel').modal('show');
                $('#id_cliente').val(data.id_cliente);
                $('#nome').val(data.nome);
                $('#cnpj').val(data.cnpj);
                $('#telefone').val(data.telefone);
                $('#responsavel').val(data.responsavel);
                $('#email').val(data.email);
            })
        });

        $('#saveBtn').click(function(e) {
            e.preventDefault();
            $(this).html('Sending..');

            $.ajax({
                data: $('#clienteForm').serialize(),
                url: "{{ route('clientes.store') }}",
                type: "POST",
                dataType: 'json',
                success: function(data) {

                    $('#clienteForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    table.draw();

                },
                error: function(data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Salvar Alterações');
                }
            });
        });

        $('body').on('click', '.deleteCliente', function() {

            var id_endereco = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "DELETE",
                url: "{{ route('clientes.store') }}" + '/' + id_endereco,
                success: function(data) {
                    table.draw();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
        //Método Delete//
        $(document).on('click', '.delete-user', function(e) {
            e.preventDefault();
            if (confirm('Tem certeza que deseja excluir?')) {
                var id = $(this).data('clientes');
                var $bt = $(this);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'delete',
                    dataType: "JSON",
                    url: 'clientes/' + id, //rota que deleta
                    beforeSend: function() {}
                }).done(function(response) {
                    console.log(response);
                    try {
                        //response = JSON.parse(response);
                        if (response.success) {
                            var $tr = $bt.closest('tr');
                            $tr.addClass('animated fadeOutLeft');
                            setTimeout(function() {
                                $tr.remove();
                            }, 1000);
                        } else {
                            alert(response.message);
                        }
                    } catch (e) {
                        alert('Ocorreu um erro desconhecido, por favor tente novamente mais tarde.',
                            'Erro!');
                    }
                });
            }
        });

    </script>


@endsection
