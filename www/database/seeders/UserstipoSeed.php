<?php

namespace Database\Seeders;

use App\Models\Userstipo;
use Illuminate\Database\Seeder;

class UserstipoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Userstipo::insert(['descricao' => 'Admin']);
        Userstipo::insert(['descricao' => 'Editor']);
        Userstipo::insert(['descricao' => 'Visualizador']);
    }
}
