<?php

namespace Database\Seeders;

use App\Models\Enderecotipo;
use Illuminate\Database\Seeder;

class EnderecotipoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Enderecotipo::insert(['descricao' => 'Principal']);
        Enderecotipo::insert(['descricao' => 'Cobrança']);
        Enderecotipo::insert(['descricao' => 'Entrega']);
        Enderecotipo::insert(['descricao' => 'Outros']);
    }
}
